<?php

/**
 * @file
 * Implements administration pages for trusted_shops module.
 */

/**
 * Callback for administration form.
 */
function commerce_trusted_shops_admin_form($form, &$form_state) {
  $form = array();

  $params = variable_get('trusted_shops_settings', NULL);
  $params = json_decode($params);

  $form['trusted_shops_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Trusted Shops ID'),
    '#description' => t('Enter Trusted Shops ID / Certification No.'),
    '#default_value' => isset($params->id) ? $params->id : '',
    '#required' => TRUE,
  );

  $form['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('TrustBadge Settings'),
    '#tree' => TRUE,
    '#prefix' => '<div id="field-settings-ajax">',
    '#suffix' => '</div>',
  );
  $form['settings']['yOffset'] = array(
    '#type' => 'textfield',
    '#title' => t('Vertical offset'),
    '#description' => t('Offset from the page bottom'),
    '#element_validate' => array('element_validate_integer'),
    '#default_value' => isset($params->yOffset) ? $params->yOffset : 0,
  );
  $form['settings']['variant'] = array(
    '#type' => 'select',
    '#title' => t('Variant'),
    '#options' => array(
      'text' => t('Text only'),
      'default' => t('Default'),
      'small' => t('Small'),
      'reviews' => t('Reviews'),
      'custom' => t('Custom'),
      'custom_reviews' => t('Custom with reviews'),
    ),
    '#default_value' => isset($params->variant) ? $params->variant : 'default',
    '#ajax' => array(
      'callback' => 'trusted_shops_admin_ajax',
      'wrapper' => 'field-settings-ajax',
    ),
  );

  if (isset($form_state['values']) && ($form_state['values']['settings']['variant'] == 'custom' || $form_state['values']['settings']['variant'] == 'custom_reviews')) {

    $form['settings']['customElementId'] = array(
      '#type' => 'textfield',
      '#title' => t('Custom Element ID'),
      '#default_value' => isset($params->customElementId) ? $params->customElementId : '',
    );
    $form['settings']['trustcardDirection'] = array(
      '#type' => 'select',
      '#title' => t('Trustcard direction'),
      '#options' => array(
        'topRight' => t('Top right'),
        'topLeft' => t('Top left'),
        'bottomRight' => t('Bottom right'),
        'bottomLeft' => t('Bottom left'),
      ),
      '#default_value' => isset($params->trustcardDirection) ? $params->trustcardDirection : 'bottomRight',
    );
    $form['settings']['customBadgeWidth'] = array(
      '#type' => 'textfield',
      '#title' => t('Custom badge width'),
      '#description' => t('Custom badge width between 40 and 90 pixels'),
      '#element_validate' => array('trusted_shops_validate_custom_badge_size'),
      '#default_value' => isset($params->customBadgeWidth) ? $params->customBadgeWidth : '',
    );
    $form['settings']['customBadgeHeight'] = array(
      '#type' => 'textfield',
      '#title' => t('Custom badge height'),
      '#description' => t('Custom badge height between 40 and 90 pixels'),
      '#element_validate' => array('trusted_shops_validate_custom_badge_size'),
      '#default_value' => isset($params->customBadgeHeight) ? $params->customBadgeHeight : '',
    );
  }

  $form['settings']['disableResponsive'] = array(
    '#type' => 'checkbox',
    '#title' => t('Disable responsive behaviour'),
    '#default_value' => isset($params->disableResponsive) ? $params->disableResponsive : FALSE,
  );
  $form['settings']['trustCardTrigger'] = array(
    '#type' => 'select',
    '#title' => t('TrustCard Trigger'),
    '#options' => array(
      'mouseover' => t('Mouseover'),
      'click' => t('Click'),
    ),
    '#default_value' => isset($params->trustCardTrigger) ? $params->trustCardTrigger : 'mouseover',
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Configuration'),
  );

  return $form;
}

/**
 * Submit callback for trusted_shops_admin_form().
 */
function commerce_trusted_shops_admin_form_submit(&$form, &$form_state) {

  $params = new stdClass();
  $params->id = $form_state['values']['trusted_shops_id'];

  foreach ($form_state['values']['settings'] as $key => $value) {
    $params->$key = $value;
  }

  $variable = json_encode($params);

  variable_set('trusted_shops_settings', $variable);
  drupal_set_message(t('Configuration saved.'), 'status');
}

/**
 * Ajax callback for trusted_shops_admin settings ajax.
 */
function commerce_trusted_shops_admin_ajax($form, $form_state) {
  return $form['settings'];
}
