(function ($, Drupal, window, document, undefined) {

Drupal.behaviors.trustBadge = {
  attach: function(context, settings) {

    $(document).ready(function() {

      // We hide the block itself, as it is empty anyway
      $('.block-trusted-shops').hide();

      var _tsid = settings.trustBadge._tsid;

      _tsConfig = {
        'yOffset': settings.trustBadge.yOffset,
        'variant': settings.trustBadge.variant,
        'customElementId': settings.trustBadge.customElementId,
        'trustcardDirection': settings.trustBadge.trustcardDirection,
        'customBadgeWidth': settings.trustBadge.customBadgeWidth,
        'customBadgeHeight': settings.trustBadge.customBadgeHeight,
        'disableResponsive': settings.trustBadge.disableResponsive,
        'disableTrustbadge': settings.trustBadge.disableTrustbadge,
        'trustCardTrigger': settings.trustBadge.trustCardTrigger
      };

      var _ts = document.createElement('script');
      _ts.type = 'text/javascript';
      _ts.charset = 'utf-8';
      _ts.async = true;
      _ts.src = '//widgets.trustedshops.com/js/' + _tsid + '.js';
      var __ts = document.getElementsByTagName('script')[0];
      __ts.parentNode.insertBefore(_ts, __ts);

    });

  }
};


})(jQuery, Drupal, this, this.document);
